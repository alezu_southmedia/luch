<?php

function get_stages($liter_dir = ''){
  $base_dir = 'images/stages/';
  $stages = scandir($base_dir.$liter_dir);
  $output = '<div class="stages-slider">';
  foreach($stages as $stage) {
    if(!is_dir($base_dir . $liter_dir . '/' . $stage) || $stage == '.' || $stage == '..') continue;
    $stage_name = explode('.',$stage);
    $output .= '
    <div class="stage">'.
            get_stage_gallery($base_dir . $liter_dir . $stage) .
    '</div>';
  }
  $output .= '</div>';
  $output .= '<div class="stage-slider-navigation">
                        <a href="#" class="slider-nav slider-nav-prev">Судак</a>
                        <a href="#" class="slider-nav slider-nav-next">Тудак</a>
                    </div>';
  return $output.'</ul>';
}

function get_stage_gallery($stage_dir){
  $dir = scandir($stage_dir);
  if(!array_search('small',$dir) || !array_search('big',$dir)) return;
  $small_dir = $stage_dir.'/small/';
  $big_dir = $stage_dir.'/big/';
  $small_images = scandir($small_dir);
  $is_first = true;
  $output = '';
  foreach($small_images as $key => $small_image){
    if(!is_file($small_dir . $small_image)) continue;
    $size = getimagesize($small_dir . $small_image);
    $stage_name = explode('/',$stage_dir);
    $stage_name = explode('.',$stage_name[count($stage_name)-1]);
    if($is_first) {
      $output .= '<div class="stage-title">' . $stage_name[1] . '</div><a href="'.$big_dir.$small_image.'" class="colorbox" title="'.$stage_name[1].'" rel="'.base64_encode($stage_dir).'">
          <img src="'.$small_dir.$small_image.'" alt="'.$stage_name[1].'" width="'.$size[0].'" height="'.$size[1].'" />
          </a>';
      $is_first = false;
    }
    else {
      $output .= '<a href="' . $big_dir . $small_image . '" class="colorbox" title="' . $stage_name[1] . '" rel="' . base64_encode($stage_dir) . '"></a>';
    }
  }

  return $output;
}

function get_gallery($stage_dir = 'images/stages'){
  $dir = scandir($stage_dir);
  if(!array_search('small',$dir) || !array_search('big',$dir)) return;
  $small_dir = $stage_dir.'/small/';
  $big_dir = $stage_dir.'/big/';
  $small_images = scandir($small_dir);
  $output = '';
  foreach($small_images as $key => $small_image){
    if(!is_file($small_dir . $small_image)) continue;
    $size = getimagesize($small_dir . $small_image);
    $stage_name = explode('/',$stage_dir);
    $stage_name = explode('.',$stage_name[count($stage_name)-1]);
    $output .= '<a href="'.$big_dir.$small_image.'" class="colorbox" title="Ход строительства ЖК «Луч»" rel="'.base64_encode($stage_dir).'">'."\n".'
          <img src="'.$small_dir.$small_image.'" alt="Ход строительства ЖК «Луч»" width="'.$size[0].'" height="'.$size[1].'" />'."\n".'
          </a>';

  }

  return $output;
}