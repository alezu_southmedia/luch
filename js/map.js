var point = new google.maps.LatLng(45.071776, 39.006546);
var center = new google.maps.LatLng(45.070776, 39.006546);

var request = {
    location: point,
    radius: 600,
    types: [
        'school',
        'hospital',
        'health',
        'pharmacy',
        'dentist',
        'doctor',
        'park',
        'sport',
        'stadium',
        'gym',
        'shopping_mall',
        'grocery_or_supermarket',
        'shopping',
        'store'
    ]
};

var additional_coords = [
{
    'type': 'park',
    'name': 'Сквер Фестивальный',
    'coords': [45.059541, 38.960140]
},
{
    'type': 'park',
    'name': 'Бульвар Солнечный',
    'coords': [45.072388,39.006137]
},
{
    'type': 'park',
    'name': 'Бульвар Солнечный',
    'coords': [45.057995,38.993127]
},
{
    'type': 'park',
    'name': 'Бульвар Солнечный',
    'coords': [45.07247,39.012087]
},
{
    'type': 'park',
    'name': 'Чистяковская роща',
    'coords': [45.057995,38.993127]
},
{
    'type': 'park',
    'name': 'Парк 60-летия Победы',
    'coords': [45.057995,38.993127]
},
{
    'type': 'park',
    'name': 'Сквер Памяти Евгения Дороша',
    'coords': [45.057995,38.993127]
},
 {
     'type': 'hospital',
     'name': 'Сеть аптек Радуга',
     'coords': [45.071358,39.004436]
 },
 {
     'type': 'shop',
     'name': 'Семейный Магнит',
     'coords': [45.071768,39.003932]
 },
 {
     'type': 'shop',
     'name': 'Тандер',
     'coords': [45.073007,39.002344]
 },
 {
     'type': 'sport',
     'name': 'Cпортивно-оздоровительный центр Дельфин',
     'coords': [45.073222,39.007008]
 },
 {
     'type': 'sport',
     'name': 'Фитнес-клуб Olympia',
     'coords': [45.067784,39.009836]
 },
 {
     'type': 'sport',
     'name': 'Фитнес-клуб Континент',
     'coords': [45.065652,38.997054]
 },
 {
     'type': 'sport',
     'name': 'Фитнес-клуб Fitness Life',
     'coords': [45.074629,39.022744]
 },
 {
     'type': 'sport',
     'name': 'Детский бассейн AquaBaby',
     'coords': [45.068257,39.013896]
 },
 {
     'type': 'sport',
     'name': 'Бассейн Родник',
     'coords': [45.056753,39.023346]
 },
 {
     'type': 'school',
     'name': 'СОШ №66',
     'coords': [45.074534,39.017588]
 },
 {
     'type': 'school',
     'name': 'СОШ №71',
     'coords': [45.064763,39.005245]
 },
 {
     'type': 'school',
     'name': 'СОШ №93',
     'coords': [45.052927,39.02234]
 },
 {
     'type': 'school',
     'name': 'СОШ №78',
     'coords': [45.052602,39.006044]
 },
 {
     'type': 'school',
     'name': 'СОШ №47',
     'coords': [45.0505,38.996774]
 },
 {
     'type': 'school',
     'name': 'СОШ №10',
     'coords': [45.057345,38.988761]
 },
 {
     'type': 'school',
     'name': 'СОШ №17',
     'coords': [5.078945,38.982958]
 },
 ]

function initialize() {
    //var map;
    var infowindow;

    var stylez = [{
        featureType: "all",
        elementType: "all",
        stylers: [{
            //saturation: -100
        }]
    }];

    var mapOptions = {
        zoom: 16,
        center: center,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
        },
        scaleControl: false,
        zoomControl: true,
        scrollwheel: false,
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var mapType = new google.maps.StyledMapType(stylez, {
        name: "Инфраструктура ЖК «Луч»"
    });
    //map.mapTypes.set('tehgrayz', mapType);
    //map.setMapTypeId('tehgrayz');

    var homeMarker = new google.maps.Marker({
        position: point,
        map: map,
        title: 'ЖК «Луч»',
        icon: {
            url: 'images/icons/icon-placemarker.png',
            size: new google.maps.Size(52, 65),
            anchor: new google.maps.Point(26, 62),
            //scaledSize: new google.maps.Size(28, 45)
        },
    });



    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.search(request, callback);
    createAdditionalMarkers(additional_coords);
}


google.maps.event.addDomListener(window, 'load', initialize);

function callback(results, status, additional_coords) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }

}



var image = 'images/icons/icon-placemarker.png';

function typeSearch(types) {
    for (var i in request.types) {
        if (types.indexOf(request.types[i]) !== -1) return request.types[i];
    }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        //map: map,
        position: place.geometry.location,
        icon: image
    });
    //marker.setMap(map);

    var type;
    type = typeSearch(place.types);
    if(type == 'store' || type == 'shopping_mall' || type == 'grocery_or_supermarket' || type == 'shopping') type = 'shop';
    if(type == 'gym' || type == 'stadium') type = 'sport';
    if((type == 'health') || (type == 'pharmacy') || (type == 'dentist') || (type == 'doctor')) type = 'hospital';
    if(place.name.indexOf('фитнес') != -1) type = 'sport';


    var infoBubble = new InfoBubble({
        map: map,
        content: '<div class="icon-placemark icon-placemark-' + type + '" title="'+place.name+'"></div>',
        position: placeLoc,
        shadowStyle: 0,
        padding: 0,
        backgroundColor: '',
        borderRadius: 0,
        arrowSize: 0,
        borderWidth: 0,
        disableAutoPan: true,
        hideCloseButton: true,
        arrowPosition: 0,
        backgroundClassName: 'marker',
        arrowStyle: 2
    });

    infoBubble.open();


    // google.maps.event.addListener(marker, 'click', function() {
    //     infowindow.setContent(place.name);
    //     infowindow.open(map, this);
    // });
}


function createAdditionalMarkers(places) {

    var place;
    for (var i = 0; i < places.length; i++) {
        place = places[i];

        var placeLoc = new google.maps.LatLng(place.coords[0], place.coords[1]);
        var marker = new google.maps.Marker({
            //map: map,
            position: placeLoc,
            icon: image
        });
        //marker.setMap(map);

        var infoBubble = new InfoBubble({
            map: map,
            content: '<div class="icon-placemark icon-placemark-' + place.type + '" title="'+place.name+'"></div>',
            position: placeLoc,
            shadowStyle: 0,
            padding: 0,
            backgroundColor: '',
            borderRadius: 0,
            arrowSize: 0,
            borderWidth: 0,
            disableAutoPan: true,
            hideCloseButton: true,
            arrowPosition: 0,
            backgroundClassName: 'marker',
            arrowStyle: 2
        });

        infoBubble.open();
    }
}

google.maps.event.addDomListener(window, 'load', initialize);