
function send(el, container) {
  var inputs = el.find('input[type="text"],input[type="tel"],input[type="email"],input[type="hidden"],textarea'),
          data;
  if (container.parents('.modal-content').find('h2').length) {
    data = 'type=' + container.parents('.modal-content').find('h2').text();
  } else if (container.parent('div').find('h3').length) {
    data = 'type=' + container.find('h3').text();
  } else {
    data = 'type=Заказать звонок';
  }
  data += '&id=' + el.attr('id');
  for (var i = 0; i < $(inputs).length; i++) {
    switch ($(inputs[i]).attr('type')) {
      case 'text':
        if ($(inputs[i]).attr('placeholder') == "First name")
          break;
        if (!$(inputs[i]).val().length) {
          $(inputs[i]).addClass('error');
          add_error($(inputs[i]));
        } else {
          $(inputs[i]).removeClass('error');
          remove_error($(inputs[i]));
        }
        data += '&' + $(inputs[i]).siblings('label').text() + '=' + $(inputs[i]).val();
        break;
      case 'hidden':
        if ($(inputs[i]).attr('placeholder') == "First name")
          break;
        data += '&' + $(inputs[i]).siblings('label').text() + '=' + $(inputs[i]).val();
        break;
      case 'tel':
        var phone = $(inputs[i]);

        if ($(phone).val() != '') {
          var pattern_phone = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,12}$/i;
          if (pattern_phone.test($(phone).val())) {
            $(phone).removeClass('error');
            remove_error($(inputs[i]));
          } else {
            $(phone).addClass('error');
            add_error($(inputs[i]));
          }
        }
        if ($(phone).val() == 0) {
          $(phone).addClass('error');
          add_error($(inputs[i]));
        } else {
          $(phone).removeClass('error');
          remove_error($(inputs[i]));
        }
        data += '&' + $(inputs[i]).siblings('label').text() + '=' + $(inputs[i]).val();
        break;
      case 'email':
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!filter.test($(inputs[i]).val())) {
          $(inputs[i]).addClass('error');
          add_error($(inputs[i]));
        } else {
          $(inputs[i]).removeClass('error');
          remove_error($(inputs[i]));
        }
        data += '&' + $(inputs[i]).siblings('label').text() + '=' + $(inputs[i]).val();
        break;
      default:
        if (!$(inputs[i]).val().length) {
          $(inputs[i]).addClass('error');
          add_error($(inputs[i]));
        } else {
          $(inputs[i]).removeClass('error');
          remove_error($(inputs[i]));
        }
        data += '&' + $(inputs[i]).siblings('label').text() + '=' + $(inputs[i]).val();
        break;

    }
  }

  if ($(el).find('.error').length > 0) {
    console.log('not valid');
  } else {
    $.ajax({
      type: "post",
      url: "send.php",
      data: data,
      success: function (msg) {
        var form_id = getFormId(data);
        container.parents('.modal').removeClass('modal-showed');
        if (!el.parents('.modal').length)
          el.hide('slow');
        $('.overlay').fadeOut('slow');

        if ($(el).parent().hasClass('modal_block')) {
          $(el).fadeOut(function () {
            $(el).parent().find('p.thnks').fadeIn();
          })
        }


        switch (form_id) {
          case 'callback-intro':
            // yaEvent('zvonok_sent');
            break;
          case 'form-view':
            // yaEvent('prosmotr');
            break;
          case 'form-callback':
            // yaEvent('more');
            break;
          case 'form-ipoteque':
            // yaEvent('ipoteka');
            break;
          case 'form-rassrochka':
            // yaEvent('rassrochka');
            break;
          case 'form-motherhood':
            // yaEvent('matkap');
            break;
          case 'form-distance':
            // yaEvent('distance');
            break;
          case 'form-callback-contacts':
            // yaEvent('zvonok2');
            break;
          case 'callback-apperitive':
            // yaEvent('sent');
            break;
        }
      }
    });
  }
}

$(document).ready(function () {
  var ua = detect.parse(navigator.userAgent);

  if (ua.browser.family == 'IE') {
    $('#section-3').css({bottom: "0","margin-bottom":"70px"});
    $('#section-6 .inner').css({bottom: "350px"});
    $('#section-7-2').css({bottom: "0","margin-bottom":"50px"});
  }

  setTimeout(function () {
    $('.modal_popup').fadeIn('slow');
  }, 8000);


  if ($('.nav').offset().top > 100) {
    $('.nav').css('background-color', '#7182D4');
  } else {
    $('.nav').css('background-color', 'transparent');
  }

  $('h1, .pre-title-title-ha-ha-ha, nav a').each(function () {
    if ($(this).html($(this).html().replace('»', '<span class="rotate">«</span>')))
      ;
  });


  $('.switcher .item a').on('click', function () {
    $(this).parents('.switcher').find('.item.active').removeClass('active');
    $(this).parent('.item').addClass('active');
    var target = $(this).attr('href');
    $(this).parents('.switcher').find('.left img').attr('src', target);
    return false;
  });
  $('.switcher-nav a').on('click', function () {
    var past_active = $(this).parents('.switcher').find('.item.active');
    if ($(this).hasClass('next')) {
      var new_active = $(this).parents('.switcher').find('.item.active').next();
      if (!new_active.length) {
        new_active = $(this).parents('.switcher').find('.item').eq(0);
      }
    }
    if ($(this).hasClass('prev')) {
      var new_active = $(this).parents('.switcher').find('.item.active').prev();
      if (!new_active.length) {
        new_active = $(this).parents('.switcher').find('.item:last-child');
      }
    }
    var target = new_active.find('a').attr('href');
    past_active.removeClass('active');
    new_active.addClass('active');
    $(this).parents('.switcher').find('.left img').attr('src', target);
    return false;
  });

  var meter_price = $('.section-plans .subsection-content').data('price');
  $('.section-plans .subsection-content .item').each(function () {
    var flat_square = $(this).parents('section').find('.subsection-header .item').eq($(this).index()).find('.value').text().replace(',', '.');
    $(this).find('.col .price-item').eq(0).find('.value').text(number_format(meter_price, 0, ',', ' '));
    $(this).find('.col .price-item').eq(1).find('.value').text(number_format(meter_price * flat_square, 0, ',', ' '));
  });

  ready_value = $('.ready-value').text();
  ready_line_animate();


  $('input:not([type="submit"])').each(function () {
    if ($(this).val() != '' && $(this).val() != '+7(___) ___-__-__') {
      $(this).addClass('filled');
    } else {
      $(this).removeClass('filled');
    }
  });

  $('input:not([type="submit"])').on('keyup blur change', function () {
    if ($(this).val() != '' && $(this).val() != '+7(___) ___-__-__') {
      $(this).addClass('filled');
    } else {
      $(this).removeClass('filled');
    }
  });

  $('[name="phone"]').mask('+7(999) 999-99-99');
  $('[name="callback_phone"]').mask('(999) 999-99-99');

  $('.render-total span').text($('.section-renders .renders .render').length);
  $('.render-current').text($('.section-renders .renders .render.selected').data('index'));

  var plan_height = 0;
  $('.subsection-content .item').each(function () {
    if ($(this).height() > plan_height)
      plan_height = $(this).height();
  });
  $('.subsection-content .items').height(plan_height);



  $('.render-nav-link').on('click', function () {
    $(this).parent('.render-nav').addClass('pressed');
    if ($(this).hasClass('render-prev')) {
      $(this).parent('.render-nav').removeClass('alternate').parents('section').find('.selected').removeClass('selected');
      $(this).parents('section').find('.render:last-of-type').addClass('selected');
    } else {
      $(this).parent('.render-nav').addClass('alternate').parents('section').find('.selected').removeClass('selected');
      $(this).parents('section').find('.render').eq(0).addClass('selected');
    }
    $('.render-current').text($('.section-renders .renders .render.stack__item--current').data('index'));

    return false;
  })

  $('.section-inside .right .item').on('click', function () {
    var index = $(this).index();
    $(this).siblings().removeClass('selected');
    $(this).parents('section').find('.left .selected').removeClass('selected');
    $(this).parents('section').find('.left .item').eq(index).addClass('selected');
    $(this).addClass('selected');
  });

  $('.section-plans .subsection-header .item').on('click', function () {
    var index = $(this).attr('class').replace('item ','').replace('big','').replace('item-','');
    $(this).parent('.items').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    $(this).parents('section').find('.subsection-content .selected').removeClass('selected');
    $(this).parents('section').find('.subsection-content .item-'+index).addClass('selected');
  });

  $('.map-legend .item').on('click', function () {
    $(this).parents('.map-legend').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var type = $(this).attr('class').replace('item item-', '').replace(' selected', '');
    if (type == 'all') {
      $('.icon-placemark').fadeIn('slow');
    } else {
      $('.icon-placemark').each(function () {
        if ($(this).hasClass('icon-placemark-' + type))
          $(this).fadeIn('slow');
        else
          $(this).fadeOut('slow')
      });
    }
  });

  $('.stages-slider').each(function () {
    $(this).carouFredSel({
      responsive: false,
      circular: false,
      infinite: false,
      height: 'auto',
      prev: $(this).parents('section').find('.slider-nav-prev'),
      next: $(this).parents('section').find('.slider-nav-next'),
      auto: {
        play: false,
        duration: 800,
        timeoutDuration: 5000,
      },
      items: {
        visible: 3,
        width: 215,
        height: 175
      },
      scroll: {
        items: 1,
        fx: 'directscroll',
      },
      swipe: {
        onTouch: true,
        onMouse: true,
        options: {
          excludedElements: "button, input, select, textarea, .noSwipe"
        }
      }
    });
  });

  $('.tcabs-wrapper').each(function () {
    var el_height = 0;
    $(this).find('.tab').each(function () {
      if ($(this).height() > el_height)
        el_height = $(this).height();
    });
    $(this).find('.tabs').height(el_height);
  });

  $('.tabs-wrapper .btns .btn').on('click', function () {
    if (!$(this).hasClass('selected')) {
      $(this).parents('.tabs-wrapper').find('.btn.selected').removeClass('selected');
      $(this).addClass('selected');
      $(this).parents('.tabs-wrapper').find('.tab.selected').removeClass('selected');
      $(this).parents('.tabs-wrapper').find('.tab').eq($(this).index()).addClass('selected');
    }
    return false;
  });



  /* Меню */
  $('nav a').on('click', function () {
    var target = $(this).attr('href');
    if (typeof (target) !== 'undefined') {
      $('html,body').scrollTo(($(target).offset().top - 150), {
        duration: 1500,
        axis: 'y'
      });
      $(this).parents('.modal').removeClass('modal-showed');
      $('.overlay').fadeOut('slow');
    }
    return false;
  });
  /* //Меню */


  /* Модалки */

  $('.plus').on('click', function () {

    var target = $(this).attr('href');

    if (typeof (target) !== 'undefined') {
      $('.modal-showed').removeClass('modal-showed');
      $(target).addClass('modal-showed');
      return false;
    }
  });


  $('.popupper').on('click', function () {

    var target = $(this).attr('href');

    if (typeof (target) !== 'undefined') {
      $('.modal-showed').removeClass('modal-showed');
      $(target).addClass('modal-showed');
      $('.overlay').fadeIn('fast');
      return false;
    }
  });

  $('.close-btn').on('click touchstart', function () {
    $(this).parents('.modal').removeClass('modal-showed');
    var _this = $(this);
    $('.overlay').fadeOut('slow');
    return false;
  });

  $('.overlay').on('click touchstart', function (event) {
    event.stopPropagation();
    $('.modal-showed').removeClass('modal-showed');
    $('.overlay').fadeOut('slow');
    $('.modal-form-success').animate({
      opacity: 0
    }, 800);
    setTimeout(function () {
      $('.modal-form-success').hide();
    }, 800);
    return false;
  });
  /* // Модалки */



  $('.colorbox').colorbox({
    scalePhotos: true,
    maxWidth: '80%',
    maxHeight: '80%',
    onOpen: function () {
      $("#colorbox").addClass("gallery-box");
    },
    onClosed: function () {
      $("#colorbox").removeClass("gallery-box");
    }
  });

  $('form').on('submit', function () {
    send($(this), $(this).parent('div'));
    return false;
  });
});


function number_format(number, decimals, dec_point, thousands_sep) {

  var i, j, kw, kd, km;
  if (isNaN(decimals = Math.abs(decimals))) {
    decimals = 2;
  }
  if (dec_point == undefined) {
    dec_point = ",";
  }
  if (thousands_sep == undefined) {
    thousands_sep = ".";
  }
  i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
  if ((j = i.length) > 3) {
    j = j % 3;
  } else {
    j = 0;
  }
  km = (j ? i.substr(0, j) + thousands_sep : "");
  kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
  //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
  kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
  return km + kw + kd;
}

function ready_line_animate() {

  if (typeof (ready_value) == 'undefined')
    ready_value = $('.ready-value').text();
  if ($('.ready-line').isOnScreen()) {
    if (!$('.ready-line').hasClass('animated')) {
      $('.ready-line').addClass('animated');
      $('.ready-value').text(0);
      var i = 0;
      var interval = setInterval(function () {
        i++;
        $('.ready-value').text(i);
        $('.ready-line').css({
          width: i + '%'
        });
        if (i >= ready_value)
          clearInterval(interval);
      }, 50);
    }
  } else {
    $('.ready-line').removeClass('animated');
  }
}

$.fn.isOnScreen = function () {
  var element = this.get(0);
  if (typeof (element) != "undefined") {
    var bounds = element.getBoundingClientRect();
    return bounds.top < window.innerHeight && bounds.bottom > 0;
  }
}


function add_error(input) {
  remove_error(input);
  error_message = '';
  if (typeof (input.siblings('label') !== 'undefined') && input.siblings('label')) {
    var txt = input.siblings('label').text();
    console.log(txt);
    if (txt.indexOf('Имя') !== -1) {
      var error_message = '<div class="error-message"><div class="icon"></div>Введите имя</div>';
    } else {
      var error_message = '<div class="error-message"><div class="icon"></div>' + txt + ' не введен</div>';
    }
  }
  $(input).after(error_message);
}

function remove_error(input) {
  input.next('.error-message').remove();
}

function yaEvent(a) {
  if (window['yaCounter33804069']) {
    yaCounter33804069.reachGoal(a);
    console.log(a + ' event has been sent...')
  }
}


$(document).keyup(function (e) {
  if (e.keyCode == 27) {
    $('.modal-showed').removeClass('modal-showed');
    $('.overlay').fadeOut('slow');
  }
});


function getFormId(data) {
  data = data.split('&');
  for (var key in data) {
    var element = data[key].split('=');
    if (element[0] == 'id') {
      return element[1];
    }
  }
}

$(document).scroll(function () {
  ready_line_animate();
});