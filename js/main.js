;
var scripts = scripts || {};

scripts.aquamarine = {

    heights: {},

    detecting: function() {
        // global technology detecting
        $('html').removeClass('no-js');
    },

    orderedList: function() {
        $('.content ol').each(function() {
            $(this).children('li').each(function(index) {
                $(this).prepend('<i class="list_ordered__counter-point-js">' + (index + 1) + ')</i>');
            });
        });
    },

    mobileVersion: function() {
        if ((navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
            $('body').addClass('page-mobile');
        }
    },

    tablesSupport: function() {
        $('.table tbody').each(function() {
            $(this).find('tr').filter(':odd').addClass('table__tr-odd');
        });
    },

    plans: function() {
        $('.genplan-item').on('click', function() {
            $('#modal-plane .modal-plane img').attr('src', '').attr('src', $(this).attr('data-src'))
        });
    },



    pagePanelBlock: function() {

        $('.js-panel-btn').on('click', function() {

            if ($(this).hasClass('js-panel-btn_open')) {
                $('.panel-page').removeClass('panel-page_open');
                $(this).removeClass('js-panel-btn_open');
                setTimeout(
                    function() {
                        $('.header__right, .site-name_block, .middle, .footer').removeClass('b-blur');
                    }, 50
                );
            } else {
                $('.panel-page').addClass('panel-page_open');
                $(this).addClass('js-panel-btn_open');
                setTimeout(
                    function() {
                        $('.header__right, .site-name_block, .middle, .footer').addClass('b-blur');
                    }, 50
                );
            }
        });
        $(document).on('click', '.middle.b-blur', function() {
            if ($('.js-panel-btn').hasClass('js-panel-btn_open')) {
                $('.panel-page').removeClass('panel-page_open');
                $('.js-panel-btn').removeClass('js-panel-btn_open');
                setTimeout(
                    function() {
                        $('.header__right, .site-name_block, .middle, .footer').removeClass('b-blur');
                    }, 50
                );
            }
        })
    },

    footerBlock: function() {
        var windowHeight = $(window).height(),
            footerHeight = $('.footer__inner').height();
        $('.wrapper').css('min-height', windowHeight);
        $('.footer').css('height', footerHeight);
        $('.footer').css('margin-top', -footerHeight);
        $('.middle').css('padding-bottom', footerHeight);
    },

    fixedBlock: function() {
        var block = $('.js-block-fixed'),
            fixedHeight = $('.js-block-fixed').height();

        $(window).scroll(function(event) {
            var y = $(this).scrollTop();
            if (y > fixedHeight) {
                block.addClass('js-block-fixed-enabled');
            } else {
                block.removeClass('js-block-fixed-enabled');
            }
        });
    },

    coverBlock: function() {
        var windowHeight = $(window).height(),
            coverHeight = $('.js-cover__inner').height(),
            headerHeight = $('.header').height();
        $('.js-cover').css('height', windowHeight);
        $('.slider .list_slider, .slider .slider-item, .slider .bx-viewport').css('height', windowHeight - headerHeight);
    },

    anchorItem: function() {
        $('.js-anchor').on('click', function(e) {

            var idscroll = $(this).attr('href'),
                destination = $(idscroll).offset().top;
            $("html,body").animate({
                scrollTop: destination
            }, 500);

            return false;

        });
    },

    setWindowHeight: function() {

        $('.b-section').each(function() {
            if ($(this).index() > 0)
                scripts.aquamarine.heights[$(this).index()] = scripts.aquamarine.heights[Number($(this).index() - 1)] + $(this).innerHeight();
            else
                scripts.aquamarine.heights[$(this).index()] = $(this).innerHeight();
        });
    },

    setActiveWindow: function() {
        var scrollTop = $(window).scrollTop();
        var iterNumber = $('.b-section').length;

        for (var i = 0; i < iterNumber; i++) {
            if (scripts.aquamarine.heights[i] > scrollTop) {
                $('.v-nav_header .v-nav__item').removeClass('v-nav__item_active');
                $('.v-nav_header .v-nav__item').eq(i).addClass('v-nav__item_active');
                break;
            }
        }
    },

    setAnchorMenuItem: function() {
        var panelHeight = $('.panel-page').height();

        $('.v-nav_header .v-nav__ln').on('click', function() {
            var idscroll = $(this).attr('href'),
                destination = $(idscroll).offset().top - 100;
            console.log(idscroll);

            $('.v-nav_header .v-nav__item').removeClass('v-nav__item_active');
            $(this).parent().addClass('v-nav__item_active');
            console.log($(this).closest('.v-nav__item'));
            $("html,body").animate({
                scrollTop: destination
            }, 500, function() {
                $('.panel-page').fadeOut();
                $('.menu.open').removeClass('open');
            });

            return false;
        });
    },

    jStylingInit: function() {
        /*if ($('.form').length) {
			$.jStyling.createSelect($('select'));
			$.jStyling.createCheckbox($('input[type=checkbox]'));
			$.jStyling.createRadio($('input[type=radio]'));
			$.jStyling.createFileInput($('input[type=file]'));
        	$.jStyling({'fileButtonText': ''})
		}*/
    },

    sliderInit: function() {
        if ($('.list_slider .list__item').length > 1) {
            var slider = $('.list_slider').bxSlider({
                mode: 'fade',
                speed: 1000,
                pause: 7000,
                pagerCustom: '.list_slider-nav',
                useCSS: false,
                easing: 'easeInCubic'
            });
        };
        return scripts;

    },

    galleryItem: function() {
        if ($('.js-list_about .list__item').length > 1) {
            var slider = $('.js-list_about').bxSlider({
                speed: 1000,
                pause: 7000
            });
        };
        return scripts;
    },

    galleryDigitItem: function() {
        if ($('.js-list-digit .list__item').length > 1) {
            var slider = $('.js-list-digit').bxSlider({
                mode: 'fade',
                speed: 1000,
                pause: 7000,
                useCSS: false,
                easing: 'easeInCubic',
                controls: false
            });
        };
        return scripts;
    },

    galleryBuild: function() {
        if ($('.js-list-gallery .list__item').length > 3) {
            var slider = $('.js-list-gallery').bxSlider({
                slideWidth: 198,
                slideHeight: 133,
                minSlides: 3,
                maxSlides: 3,
                slideMargin: 18,
                infiniteLoop: false,
                hideControlOnEnd: false,
                useCSS: false,
                speed: 500,
                pager: false,
                /*
                				easing: 'easeInCubic'*/
            });
        };
        return scripts;
    },

    fancyboxModal: function() {
        $(".js-fancybox").each(function() {

            var fancyHref = '';

            if ($(this).hasClass("js-btn-callback")) {
                fancyHref = 'include/forms/faq.php';
            }


            if ($(this).length > 0) {
                $(this).fancybox({
                    padding: 0,
                    margin: [53, 0, 0, 0],
                    fitToView: false,
                    autoWidth: true,
                    height: 'auto',
                    autoSize: false,
                    closeClick: false,
                    arrows: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    //href 		: fancyHref,
                    //type		: 'ajax',
                    helpers: {
                        overlay: {
                            css: {
                                'background': 'rgba(0,0,0,0.65)',
                                'display': 'none'
                            }
                        },
                        wrapp: {
                            css: {
                                'width': 1190
                            }
                        }
                    },
                    afterLoad: function() {
                        $('.header__right, .site-name_block, .middle, .footer').addClass('b-blur');
                    },
                    'beforeLoad': function() {
                        $('.modal-block__main, fieldset').show();
                        $('.modal-block__main .form__input').val('');
                        $('.modal-block__message').hide();

                    },
                    afterClose: function() {
                        $('.header__right, .site-name_block, .middle, .footer').removeClass('b-blur');
                        $('.modal-block__main').css('background-image', '')
                        $('.modal-block__message__content').fadeOut();
                    },
                });
            };
        });
    },



    fancyboxImg: function() {
        var fbImg = $(".js-fancybox-img");

        if (fbImg.length > 0) {
            fbImg.fancybox({
                padding: 0,
                margin: [53, -115, 0, -115],
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                width: '1190px',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(0,0,0,0.65)',
                            'display': 'none'
                        }
                    },
                },
                afterLoad: function() {
                    $('.header__right, .site-name_block, .middle, .footer').addClass('b-blur');
                },
                afterClose: function() {
                    $('.header__right, .site-name_block, .middle, .footer').removeClass('b-blur');

                }
            });

            fbImg.each(function() {
                if ($(this).find('img').length > 0) {
                    $(this).append('<i class="js-fancybox-img__ico"></i>');
                };
            });
        }


        $('.flat').click(function() {
            var rooms, square_summ, square_room, square_kuhnya, square_vannaya, square_prihozhaya, square_balkon, src, price_metr, price_full;
            rooms = $(this).attr('data-rooms');
            square_summ = $(this).find('.circle span').text();
            square_room = $(this).attr('data-sq__summ');
            square_kuhnya = $(this).attr('data-sq__kuhn');
            square_vannaya = $(this).attr('data-sq__vann');
            square_prihozhaya = $(this).attr('data-sq__prih');
            square_balkon = $(this).attr('data-sq__balk');
            price_metr = $(this).attr('data-price__m');
            price_full = $(this).attr('data-price__f');
            src = $(this).attr('data-src');

            $('#modal-plane .rooms span').text(rooms);
            $('#modal-plane .square span').html(square_summ + ' м<sup>2</sup>');
            $('#modal-plane .komnata span').text(square_room);
            $('#modal-plane .kuhnya span').text(square_kuhnya);
            $('#modal-plane .vannaya span').text(square_vannaya);
            $('#modal-plane .prihozhaya span').text(square_prihozhaya);
            $('#modal-plane .balkon span').text(square_balkon);
            $('#modal-plane .modal-plane__order  .price span').html(price_metr + ' м<sup>2</sup>');
            $('#modal-plane .modal-plane__order  .summ span').html(price_full + ' м<sup>2</sup>');
            $('#modal-plane .img img').attr('src', src);
            $('#modal-plane .download a').attr('href', src).attr('download', src);
        })
    },

    fixedHeight: function() {
        $('.list_advantage').each(function() {
            var _maxMainHeight = $(this).find('.advantage-item:first .advantage-item__preview').height();

            $(this).find('.advantage-item').each(function() {
                var _mainHeight = $(this).find('.advantage-item__preview').height();

                if (_mainHeight > _maxMainHeight)
                    _maxMainHeight = _mainHeight;

            });

            $(this).find('.advantage-item .advantage-item__preview').height(_maxMainHeight).css('line-height', _maxMainHeight + 'px');
        });

        $('.list_location').each(function() {
            var _maxMainHeight = $(this).find('.location-item:first .location-item__pic-wrap').height();

            $(this).find('.location-item').each(function() {
                var _mainHeight = $(this).find('.location-item__pic-wrap').height();

                if (_mainHeight > _maxMainHeight)
                    _maxMainHeight = _mainHeight;

            });

            $(this).find('.location-item .location-item__pic-wrap').height(_maxMainHeight).css('line-height', _maxMainHeight + 'px');
        });
        scripts.aquamarine.setWindowHeight();
    },

    fixingBlock: function() {
        // requestAnimationFrame polyfill by Erik Möller
        // fixes from Paul Irish and Tino Zijdel
        (function() {
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];
            for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
            }

            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() {
                            callback(currTime + timeToCall);
                        },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };

            if (!window.cancelAnimationFrame)
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
        }());
    },


    formsMessage: function() {
        $('.modal-block__message').hide();
        $('.form').submit(function() {
            var el = this;
            var inputs = $(this).find('input[type="text"],input[type="tel"],input[type="email"]');
            data = 'type=' + $(this).find('.form__set__legend').text();
            data += '&Источник=' + $(this).find('input[name="source"]').val();
            for (var i = 0; i < $(inputs).length; i++) {
                switch ($(inputs[i]).attr('type')) {
                    case 'text':
                        if (!$(inputs[i]).val().length) {
                            $(inputs[i]).parent().addClass('form__item_error').append('<div class="form__msg form__msg_error"><p class="form__msg__content">Ошибка ввода</p></div>');
                        } else {
                            $(inputs[i]).parent().removeClass('form__item_error').addClass('valid').find('.form__msg_error').remove();
                        }
                        data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                        break;
                    case 'tel':
                        var phone = $(inputs[i]);

                        if ($(phone).val() != '') {
                            var pattern_phone = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,12}$/i;
                            if (pattern_phone.test($(phone).val())) {
                                $(phone).parent().removeClass('form__item_error').addClass('valid').find('.form__msg_error').remove();
                            } else {
                                $(phone).parent().addClass('form__item_error').append('<div class="form__msg form__msg_error"><p class="form__msg__content">Ошибка ввода</p></div>');
                            }
                        }
                        if ($(phone).val() == 0) {
                            $(phone).parent().addClass('form__item_error').append('<div class="form__msg form__msg_error"><p class="form__msg__content">Ошибка ввода</p></div>');
                        } else {
                            $(phone).parent().removeClass('form__item_error').addClass('valid').find('.form__msg_error').remove();
                        }
                        data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                        break;
                    case 'email':
                        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        console.log($(phone).val());

                        if (!filter.test($(inputs[i]).val())) {
                            $(inputs[i]).parent().addClass('form__item_error').append('<div class="form__msg form__msg_error"><p class="form__msg__content">Ошибка ввода</p></div>');
                        } else {
                            $(inputs[i]).parent().removeClass('form__item_error').addClass('valid').find('.form__msg_error').remove();
                        }
                        data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                        break;

                }
            }
            if ($(el).find('.form__item_error').length > 0) {
                console.log('not valid');
            } else {
                console.log('valid');
                $.ajax({
                    type: "post",
                    url: "index.html",
                    data: data,
                    success: function(msg) {
                        $(el).parent().find('.modal-block__main, .modal-block__content, fieldset').fadeOut(function() {
                            $(el).parent().find('.modal-block__message__content').fadeIn();
                            $(el).closest('.modal-block__main').css('background-image', 'url(images/back_modal.png)')
                        });

                        setTimeout(
                            function() {
                                $(el).find('.modal-block__img').removeClass('modal-block__img_animate');
                            }, 15
                        );
                    }
                });
            }
            return false;
        });
    },


    tabsInit: function() {
        $tabContainer = $('.tabs');

        if (!$tabContainer.length) return;

        $tabContainer.find('.tabs__nav .tabs__item').each(function(i) {

            $('.tabs__data').hide();
            $('.tabs__data:first').show();
            $('.tabs__data:first').addClass('tabs__data_active');

            $(this).on('click', function() {

                if ($(this).hasClass('tabs__item_active')) return false;

                $tabContainer.find('.tabs__item').removeClass('tabs__item_active');
                $(this).addClass('tabs__item_active');

                $tabContainer.find('.tabs__data:visible').hide().removeClass('tabs__data_active');
                $tabContainer.find('.tabs__data').eq(i).fadeIn().addClass('tabs__data_active');

                scripts.aquamarine.setWindowHeight();

            });
        });
        // return false;
    },

    parallaxItem: function() {
        $('div[data-type="background"]').each(function() {
            var bgobj = $(this), // создаем объект
                destination = $(this).offset().top,
                topOfWindow = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop,
                heightWindow = $(window).height(),
                defaultCoords = bgobj.css('background-position');

            if (topOfWindow > destination - heightWindow) {
                var yPos = -((topOfWindow - destination) / bgobj.data('speed')); // вычисляем коэффициент
                // Присваиваем значение background-position
                var coords = "bottom" + ' ' + yPos + 'px';
                // Создаем эффект Parallax Scrolling
                bgobj.css({
                    backgroundPosition: coords
                });
            }
        });

        $('div[data-type="margin"]').each(function() {
            var mobj = $(this), // создаем объект
                destination = $(this).offset().top,
                topOfWindow = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop,
                heightWindow = $(window).height();


            if (topOfWindow > destination - heightWindow) {
                var yPos = ((topOfWindow - destination) / mobj.data('speed')); // вычисляем коэффициент
                // Присваиваем значение background-position
                var coords = yPos + 'px';
                // Создаем эффект Parallax Scrolling

                mobj.css({
                    marginTop: coords
                });
            } else {

                mobj.css({
                    marginTop: 0
                });

            }
        });
    },

    animateItem: function() {

        $('.location-item__pic-wrap').each(function() {
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (topOfWindow >= imagePos - ($(window).height())) {
                $(this).addClass("flipInY");
            } else {
                $(this).removeClass("flipInY");
            }
        });

        $('.blocks .block').each(function() {
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (topOfWindow >= imagePos - ($(window).height())) {
                $(this).addClass("fadeInUp");
            }
        });
        $('.progress .line').each(function() {
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (topOfWindow >= imagePos - ($(window).height())) {
                $('.progress .line').css('width', $('.progress .line').attr('data-percent') + '%')
            }
        });
        //

        $('.block_info .pull-left, .section-2 .block_info').each(function() {
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (topOfWindow >= imagePos - ($(window).height())) {
                $(this).addClass("slideRight");
            } else {
                $(this).removeClass("slideRight");
            }
        });
        $('.block_info .pull-right, .section-10 .block_info').each(function() {
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (topOfWindow >= imagePos - ($(window).height())) {
                $(this).addClass("slideLeft");
            } else {
                $(this).removeClass("slideLeft");
            }
        });
    },

    counter: function() {
        $(".counter").counterUp({
            delay: 10,
            time: 500
        });
    },

    maskInput: function() {
        $('.mask-phone').mask('+7 (999) 999-99-99');
    },

    galleryWrap: function() {
        var galleryText = $('.js-switch-btn').text();
        $('.js-switch-btn').on('click', function(e) {

            if ($('.gallery-wrap_1').hasClass('i-hide')) {
                $(this).text(galleryText);
                $('.gallery-wrap_1').removeClass('i-hide');
                $('.gallery-wrap_1').addClass('gallery-wrap_animate');
                $('.gallery-wrap_2').addClass('i-hide');
                $('.gallery-wrap_2').removeClass('gallery-wrap_animate');
            } else {
                $(this).text('посмотреть литер 1');
                $('.gallery-wrap_2').removeClass('i-hide');
                $('.gallery-wrap_2').addClass('gallery-wrap_animate');
                $('.gallery-wrap_1').addClass('i-hide');
                $('.gallery-wrap_1').removeClass('gallery-wrap_animate');
            }

        });
    },

    btnscrollUp: function() {
        $('.btn-scroll-up').on('click', function() {
            $('body, html').animate({
                scrollTop: 0
            }, 500);
        });
    },

    scrollRAF: function(func) {
        var latestKnownScrollY = 0,
            ticking = false,
            stickyBlock;

        stickyBlock = function() {
            if (latestKnownScrollY >= $(window).height()) {
                $('.btn-scroll-up').addClass('btn-scroll-up_active');
            } else {
                $('.btn-scroll-up').removeClass('btn-scroll-up_active');
            }
            if (latestKnownScrollY >= $('.wrapper').height() - $(window).height()) {
                $('.btn-scroll-up').css('bottom', $('.footer').height());
            } else {
                $('.btn-scroll-up').css('bottom', 0);
            }
        };

        function onScroll() {
            latestKnownScrollY = $(window).scrollTop();
            requestTick();
        }

        function requestTick() {
            if (!ticking) {
                requestAnimationFrame(update);
            }
            ticking = true;
        }

        function update() {
            ticking = false;

            stickyBlock($('.btn-scroll-up'), 'btn-scroll-up_active', 500);
        }

        $(window).on('scroll', onScroll);
    },
    //
    //mapMarker:function(){
    //
    //
    //	var map;
    //	var point = new google.maps.LatLng(43.4936579,39.8917878);
    //	var center = new google.maps.LatLng(43.494157, 39.891923);
    //
    //	var stylez = [{
    //		featureType: "all",
    //		elementType: "all",
    //
    //	}];
    //
    //	var kinders = new google.maps.MarkerImage('images/icons/kinders.png',
    //			new google.maps.Size(34, 48),
    //			new google.maps.Point(0, 0),
    //			new google.maps.Point(17, 48));;
    //	var caffes = new google.maps.MarkerImage('images/icons/coffes.png',
    //			new google.maps.Size(34, 48),
    //			new google.maps.Point(0, 0),
    //			new google.maps.Point(17, 48));
    //	var shops = new google.maps.MarkerImage('images/icons/shops.png',
    //			new google.maps.Size(34, 48),
    //			new google.maps.Point(0, 0),
    //			new google.maps.Point(17, 48));
    //
    //	var mapOptions = {
    //		zoom: 17,
    //		center: center,
    //		mapTypeControlOptions: {
    //			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
    //		},
    //		scaleControl: false,
    //		zoomControl: true,
    //		scrollwheel: false,
    //	};
    //
    //	map = new google.maps.Map(document.getElementById("map1"), mapOptions);
    //
    //
    //	var homeMarker = new google.maps.Marker({
    //		position: point,
    //		map: map,
    //		title: 'Здесь!',
    //		icon: new google.maps.MarkerImage('images/icons/atlanta.png',
    //				new google.maps.Size(46, 64), //size
    //				new google.maps.Point(0, 0), //origin point
    //				new google.maps.Point(32, 64)),
    //	});
    //
    //
    //	var shop1 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.493937, 39.889928),
    //		map: map,
    //		icon: shops,
    //	});
    //	var shop2 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.494044,39.8887599),
    //		map: map,
    //		icon: shops,
    //	});
    //	var shop2 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.4981502,39.8933693),
    //		map: map,
    //		icon: shops,
    //	});
    //	var shop2 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.494422, 39.898886),
    //		map: map,
    //		icon: shops,
    //	});
    //
    //
    //	var kinders1 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.495953, 39.893717),
    //		map: map,
    //		icon: kinders,
    //	});
    //
    //
    //	var caffes1 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.493590, 39.889222),
    //		map: map,
    //		icon: caffes,
    //	});
    //	var caffes2 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.494269, 39.888198),
    //		map: map,
    //		icon: caffes,
    //	});
    //	var caffes3 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.494715, 39.888537),
    //		map: map,
    //		icon: caffes,
    //	});
    //	var caffes4 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.494203, 39.887113),
    //		map: map,
    //		icon: caffes,
    //	});
    //	var caffes5 = new google.maps.Marker({
    //		position: new google.maps.LatLng(43.492264, 39.890459),
    //		map: map,
    //		icon: caffes,
    //	});
    //
    //
    //
    //
    //
    //	var map;
    //	var point = new google.maps.LatLng(43.4936579,39.8917878);
    //	var center = new google.maps.LatLng(43.492681777432, 39.889949172808);
    //
    //	var stylez = [{
    //		featureType: "all",
    //		elementType: "all",
    //
    //	}];
    //
    //	var kinders = new google.maps.MarkerImage('images/icons/kinders.png',
    //			new google.maps.Size(34, 48),
    //			new google.maps.Point(0, 0),
    //			new google.maps.Point(17, 48));;
    //	var caffes = new google.maps.MarkerImage('images/icons/coffes.png',
    //			new google.maps.Size(34, 48),
    //			new google.maps.Point(0, 0),
    //			new google.maps.Point(17, 48));
    //	var shops = new google.maps.MarkerImage('images/icons/shops.png',
    //			new google.maps.Size(34, 48),
    //			new google.maps.Point(0, 0),
    //			new google.maps.Point(17, 48));
    //
    //	var mapOptions = {
    //		zoom: 17,
    //		center: center,
    //		mapTypeControlOptions: {
    //			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
    //		},
    //		scaleControl: false,
    //		zoomControl: true,
    //		scrollwheel: false,
    //	};
    //
    //
    //
    //	var map2 = new google.maps.Map(document.getElementById("map2"), mapOptions);
    //
    //
    //	var homeMarker2 = new google.maps.Marker({
    //		position: point,
    //		map: map2,
    //		title: 'Здесь!',
    //		icon: new google.maps.MarkerImage('images/icons/atlanta.png',
    //				new google.maps.Size(46, 64), //size
    //				new google.maps.Point(0, 0), //origin point
    //				new google.maps.Point(32, 64)),
    //	});
    //
    //
    //
    //
    //
    //
    //},

    menu: function() {
        $(document).on('click', '.menu:not(.open)', function() {
            $('.menu').addClass('open');
            $('.panel-page').fadeIn();
        })
        $(document).on('click', '.menu.open', function() {
            $('.menu').removeClass('open');
            $('.panel-page').fadeOut();
        })
    },
    scrollClouds: function() {
        $(window).scroll(function() {

            var scroll_1 = 17;
            scroll_1 -= ($(window).scrollTop() / 100);
            $('.cloud-1').css('margin-top', scroll_1 + '%')
            var scroll_2 = 35;
            scroll_2 -= ($(window).scrollTop() / 50);
            $('.cloud-2').css('margin-top', scroll_2 + '%')
        })
    },

    init: function() {
        var scripts = this;

        scripts.detecting();

        if (!window.Modernizr || !Modernizr.generatedcontent) {
            scripts.orderedList();
        }

        if (!window.Modernizr || !Modernizr.css_lastchild) {
            scripts.tablesSupport();
        }
        scripts.footerBlock();
        scripts.coverBlock();
        scripts.pagePanelBlock();
        scripts.fixedBlock();
        scripts.sliderInit();
        scripts.galleryItem();
        scripts.anchorItem();
        scripts.galleryDigitItem();
        scripts.galleryBuild();
        scripts.setAnchorMenuItem();
        scripts.setWindowHeight();
        scripts.setActiveWindow();
        scripts.plans();


        scripts.fancyboxModal();
        scripts.fancyboxImg();

        scripts.jStylingInit();
        scripts.formsMessage();
        scripts.tabsInit();
        scripts.fixingBlock();
        scripts.counter();
        scripts.galleryWrap();
        scripts.maskInput();
        scripts.scrollRAF();
        scripts.btnscrollUp();
        //scripts.mapMarker();
        scripts.menu();
        scripts.scrollClouds();

        $(window).load(function() {
            scripts.mobileVersion();
            scripts.fixedHeight();
        });

        $(window).scroll(function() {
            scripts.setActiveWindow();
            scripts.parallaxItem();
            scripts.animateItem();

            if ($('.nav').offset().top > 100) {
                $('.nav').css('background-color', '#7182D4');
            } else {
                $('.nav').css('background-color', 'transparent');
            }

        });

        $(window).resize(function() {
            scripts.footerBlock();
            scripts.coverBlock();
            scripts.fixedBlock();
            scripts.fixedHeight();
        });

        return scripts;
    }
};

$(document).ready(function() {
    scripts.aquamarine.init();
});
