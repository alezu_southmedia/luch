<?php

$phones = array(
    'google_search' => array('source' => 'Поисковая выдача Google заявка', 'number' => '+7 (861) 238-85-32'),
    'google_adwords' => array('source' => 'Google adwords заявка', 'number' => '+7 (861) 238-83-54'),
    'yandex_direct' => array('source' => 'Яндекс директ заявка', 'number' => '+7 (861) 238-85-51'),
    'yandex_search' => array('source' => 'Поисковая выдача Яндекс заявка', 'number' => '+7 (861) 204-08-86'),
    'default' => array('source' => 'Прямой заход на сайт заявка', 'number' => '+7 (861) 238-87-25')
    );
    if(isset($_GET['rs'])) {
      $res = explode('_', $_GET['rs']);
      switch ($res[0]) {
        case 'google':
          switch ($_GET['utm_campaign']) {
            case 'g':
            case 's':
                if(isset($_GET['utm_content']))
                  $phone = $phones['google_adwords'];
                else
                  $phone = $phones['google_search'];
              break;
            case 'd':
              $phone = $phones['google_media'];
              break;
            default:
              $phone = $phones['default'];
              break;
          }
          break;
        case 'direct7':
          $phone = $phones['yandex_direct'];
          break;
        case 'bayan2':
          $phone = $phones['bayan'];
          break;
        case 'adwords9':
          $phone = $phones['google_adwords'];
          break;
        default:
          $phone = $phones['default'];
          break;
      }
    } elseif (isset($_SERVER["HTTP_REFERER"])) {
      $referer = $_SERVER["HTTP_REFERER"];
      if (substr($referer, 7, 6) == 'yandex' || substr($referer, 8, 6) == 'yandex') {
        $phone = $phones['yandex_search'];
      } elseif (strstr($referer, 'google')) {
        $phone = $phones['google_search'];
      }
    }
   if (isset($phone)) {
    setcookie('big_calltracking_phone', $phone['number'], time() + 60 * 60 * 24 * 365);
    setcookie('big_calltracking_source', $phone['source'], time() + 60 * 60 * 24 * 365);
   } else {
    $phone = $phones['default'];
   }

   if(isset($_COOKIE['big_calltracking_phone'])) {
    $phone['number'] = $_COOKIE['big_calltracking_phone'];
    $phone['source'] = $_COOKIE['big_calltracking_source'];
   }

//  $phone['number'] = '+7 (861) 244-44-94'; // не убирать. это дефолтный номер, который используется в аварийных случаях

$page['title'] = 'Жилой комплекс «Луч»';
$page['description'] = '';
$page['name'] = $page['title'];
include 'gallery.php';
?><!DOCTYPE html>

<!--[if IE]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js"></script>
<![endif]-->
<!--[if IEMobile 7]>
<html class="iem7 no-js" lang="ru" dir="ltr"><![endif]-->
<!--[if (lte IE 6)&(!IEMobile)]>
<html class="ie6 ie6-7 ie6-8 no-js" lang="ru" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]>
<html class="ie7 ie6-7 ie6-8 no-js" lang="ru" dir="ltr"><![endif]-->
<!--[if (IE 8)&(!IEMobile)]>
<html class="ie8 ie6-8 no-js" lang="ru" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!-->
<html class="no-js" lang="ru" dir="ltr" itemscope itemtype="http://schema.org/Thing">
  <!--<![endif]-->

  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=5, IE=8, IE=9, IE=10" />
    <!-- Social meta schema.org -->
    <meta itemprop="name" content="<?php print $page['name']; ?>" />
    <meta itemprop="description" content="<?php print $page['description']; ?>" />
    <meta itemprop="image" content="images/logo.png" />
    <!-- Social meta schema.org -->
    <!-- Social meta ogp.me -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://<?php print $_SERVER["HTTP_HOST"] ?>">
    <meta property="og:title" content="<?php print $page['title']; ?>" />
    <meta property="og:description" content="<?php print $page['description']; ?>" />
    <meta property="og:image" content="images/logo.png" />
    <meta property="og:site_name" content="<?php print $page['name']; ?>">
    <meta name="viewport" content="width=960">
    <!-- Social meta ogp.me -->
    <title><?php print $page['title'];?></title>
    <meta name="description" content="<?php print $page['description']; ?>" />

    <link href="http://allfont.ru/allfont.css?fonts=futura-normal" rel="stylesheet" type="text/css" />
    <link href="http://allfont.ru/allfont.css?fonts=futura-bold" rel="stylesheet" type="text/css" />
    <link href="css/colorbox.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" charset="utf-8" src="js/modernizr.custom.js"></script>
    <script src="js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
    <!--[if lt IE 9]>
        <script type="text/javascript" charset="utf-8" src="js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body itemscope itemtype="http://schema.org/LocalBusiness" class="page-index">
    <header id="section-0" class="page-header">
      <div class="nav" style="position: fixed; width: 100%; top: 0; left: 0;">
        <div class="content">
          <a href="http://ostrinskiy.com/" target="_blank" class="logo">
            <img src="images/logo.png" alt="СИК Остринский">
          </a>
          <a href="tel:<? echo $phone['number']; ?>" class="tel"><? echo $phone['number']; ?> </a>
          <a href="#modal-callback-header" class="call_back popupper">Обратный звонок</a>
          <div class="menu popupper" href="#menu"></div>
        </div>
      </div>
    </header>
    <main>
      <section id="section-1">
        <div class="cloud-1"></div>
        <div class="cloud-2"></div>
        <div class="content">
          <h1>Жилой комплекс &laquo;Луч&raquo;</h1>
          <p class="start-price"><strong>Квартиры от 1 160 000&nbsp;руб.</strong></p>
          <p class="location">Краснодар, ЗИП, ул.Солнечная, 18/5</p>
          <p class="desc">&laquo;Луч&raquo; &mdash; 24-этажный дом
            <br/>комфорт-класса, расположенный
            <br/>в благоприятном для проживания
            <br/>районе Краснодара, на улице
            <br/>Солнечной.</p>
          <a class="btn yellow popupper" href="#modal-ask-more">Узнать больше</a>
          <div class="scroll">
            Листайте вниз
            <div class="mouse">
              <div class="wheel"></div>
            </div>
            <div><span class="unu"></span> <span class="doi"></span> <span class="trei"></span> </div>
          </div>
          <div class="about">
            <h2>О комплексе</h2>
            <div class="grid">
              <div class="row clearfix">
                <div class="left">Город</div>
                <div class="right">Краснодар</div>
              </div>
              <div class="row clearfix">
                <div class="left">Район</div>
                <div class="right">ЗИП</div>
              </div>
              <div class="row clearfix">
                <div class="left">Адрес</div>
                <div class="right">ул. Солнечная, 18/5</div>
              </div>
              <div class="row clearfix">
                <div class="left">Сдача</div>
                <div class="right">4 квартал 2016</div>
              </div>
              <div class="row clearfix">
                <div class="left">Тип дома</div>
                <div class="right">монолит</div>
              </div>
              <div class="row clearfix">
                <div class="left">Этажность</div>
                <div class="right">24</div>
              </div>
              <div class="row clearfix">
                <div class="left">Площадь</div>
                <div class="right">от 28,37 м<sup>2</sup></div>
              </div>
              <?php /*<div class="row clearfix">
                <div class="left">Стоимость</div>
                <div class="right">от 1 325 280 р.</div>
              </div>*/ ?>
            </div>
          </div>
          <div class="pluses">
            <a class="plus plus-1" href="#modal-fasade">Вентилируемый фасад</a>
            <a class="plus plus-2" href="#modal-firm-walls">Прочные стены</a>
            <!-- <a class="plus plus-3" href="#modal-cosy-plans">Удобные планировки</a> -->
            <a class="plus plus-4" href="#modal-porches">Современные подъезды</a>
          </div>
        </div>
      </section>
      <section id="section-2">
        <div class="content clearfix">
          <h2>Дом высокого класса</h2>
          <div class="blocks-wrapper">
            <div class="text pull-left">
              <p>Здесь удобно жить, в удовольствие &mdash;
                <br/>растить детей, приятно ходить за
                <br/>покупками. Высокий класс и отличное
                <br/>месторасположение делает эту
                <br/>недвижимость желанной, а цены
                <br/>от застройщика &laquo;Остринский&raquo; &mdash;
                <br/>доступной.</p>
              <p>&laquo;Луч&raquo; света и уюта &mdash; это ваш новый
                <br/>дом, где всегда тепло.</p>
            </div>
            <div class="blocks-block pull-left">
              <div class="blocks">
                <div class="block block-1">
                  высокая
                  <br/> пожаробезопасность
                </div>
                <div class="block block-2">
                  Хорошая
                  <br/> звукоизоляция
                </div>
                <div class="block block-3">
                  экологичные
                  <br/> стройматериалы
                </div>
                <div class="block block-4">
                  интересный
                  <br/> архитектурный проект
                </div>
              </div>
            </div>
          </div>
        <div class="center"><a href="#modal-form-high-class" class="btn popupper blue">Узнать больше</a></div>
        </div>
      </section>
      <section id="section-3" class="parallaxy-animate" parallaxy-options='{"multiplier":"0.4", "direction":"up", "positionType": "relative"}' style="bottom: -900px;">
        <div class="shadow-block"></div>
        <div class="content">
          <div class="switcher">
            <div class="left"><img src="images/in-flat-4.jpg" alt=""></div>
            <div class="right">
              <h2>В квартирах</h2>
              <div class="items">
                <?php /*<div class="item"><a href="images/in-flat-1.jpg"><span><span>Электроразводка</span></span></a></div>*/?>
                <?php /*<div class="item"><a href="images/in-flat-2.jpg"><span><span>Водопроводные трубы</span></span><br><span><span>и счетчики воды</span></span></a></div>*/?>
                <div class="item"><a href="images/in-flat-3.jpg"><span><span>Радиаторы отопления</span></span></a></div>
                <div class="item active"><a href="images/in-flat-4.jpg"><span><span>Металлическая входная дверь</span></span></a></div>
                <div class="item"><a href="images/in-flat-5.jpg"><span><span>Металлопластиковые окна</span></span></a></div>
                <?php /*<div class="item"><a href="images/in-flat-6.jpg"><span><span>Противопожарная</span></span><br><span><span>сигнализация</span></span></a></div>*/?>
                <div class="item"><a href="images/in-flat-7.jpg"><span><span>Витражные окна</span></span></a></div>
                <div class="item"><a href="images/in-flat-8.jpg"><span><span>Остекленные лоджии</span></span></a></div>
              </div>
              <div class="switcher-nav">
                <a href="#" class="prev">Судак</a>
                <a href="#" class="next">Тудак</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="section-4">
        <div class="yellow_block">
          <div class="content">
            <div class="lift">
              <div class="cover">
                <div class="inner">
                  <h3>В доме</h3>
                  <div class="counter-wrapper">
                    <div class="counter">3</div>
                    <p class="desc">Бесшумных финских
                      <br/>лифта OTIS
                      <br/>(1 пассажирский и 2 грузовых)</p>
                  </div>
                </div>
              </div>
              <div class="blocks">
                <div class="block block-1">
                  <div class="ico"></div>
                  Мусоропровод
                </div>
                <div class="block block-2">
                  <div class="ico"></div>
                  Телефонная<br>и теливизионная сети
                </div>
                <div class="block block-3">
                  <div class="ico"></div>
                  Выделенная
                  <br>интернет-линия
                </div>
                <div class="block block-4">
                  <div class="ico"></div>
                  Коммерческая недвижимость на первых трех этажах<br>
                  <span>(с отдельными входами, витражными окнами<br>и свободной планировкой)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="section-5" class="section-closed">
        <div class="content">
          <h2><span>На закрытой территории</span></h2>
          <div class="items">
            <div class="item item-1">
              <div class="icon"></div>
              <p>Детская и спортивная
                <br>площадки</p>
            </div>
            <div class="item item-2">
              <div class="icon"></div>
              <p>Подземный
                <br>паркинг</p>
            </div>
            <div class="item item-3">
              <div class="icon"></div>
              <p>Гостевая
                <br>парковка</p>
            </div>
            <div class="item item-4">
              <div class="icon"></div>
              <p>Ландшафтное
                <br>озеленение</p>
            </div>
          </div>
          <div class="center"><a href="#modal-form-closed-territory" class="btn popupper blue">Заказать звонок</a></div>
        </div>
      </section>
      <section id="section-6" class="section-joy">
        <div class="content">
          <div class="inner parallaxy-animate" parallaxy-options='{"multiplier":"0.35", "direction":"up", "positionType": "relative"}' style="bottom:-1500px;">
            <h2>Больше света &ndash;<br>больше радости</h2>
            <p>Что такое витражное остекление? Это окна от пола до потолка!
              <br>Наслаждайтесь видами, превращайте каждое чаепитие во вдохновляющий ритуал, всегда будьте в хорошем настроении —
              <br>в ЖК «Луч» это входит в привычку.</p>
          </div>
        </div>
      </section>
      <section id="section-7" class="section-fortress">
        <div class="content">
          <div class="pic">
            <img src="images/fortress.jpg" alt="">
          </div>
          <div class="inner">
            <h2>Ваш дом &dash;<br>ваша крепость</h2>
            <p>Создавая привлекательный дом, мы помнили о&nbsp;главном&nbsp;— о безопасности тех, кто будет здесь жить. Поэтому мы огородили территорию, внедрили систему видеонаблюдения и организовали консьерж-сервис. Живите и ни о чем не беспокойтесь!</p>
          </div>
        </div>
      </section>
      <section id="section-7-2" class="section-yard parallaxy-animate" parallaxy-options='{"multiplier":"0.4", "direction":"up", "positionType": "relative"}' style="bottom: -2200px;">
        <div class="shadow-block"></div>
        <div class="content">
          <div class="inner">
            <div class="icon"></div>
            <h2>Любимый двор</h2>
            <p>На закрытой придомовой территории легко почувствовать себя как дома. Можно погулять, потренироваться на современной спортивной площадке или просто отдохнуть. А довольные детки будут рядом, на горке или в песочнице.</p>
          </div>
          <div class="pic">
            <img src="images/child.jpg" alt="">
          </div>
        </div>
      </section>
      <section id="section-8" class="section-plans">
        <header class="subsection subsection-header">
          <div class="content">
            <h2>Планировки</h2>
            <p>Еще одна особенность жилого комплекса — разнообразие планировок.
              <br>Здесь обязательно найдется квартира, подходящая именно вам.</p>
            <div class="items">
              <div class="item item-5 selected">
                <div class="label">1-комнатные</div>
                <div class="value">43,80</div>
              </div>
              <div class="item item-6 big">
                <div class="label">2-комнатные</div>
                <div class="value">76,23</div>
              </div>
              <div class="item item-1 big">
                <div class="label">Студии</div>
                <div class="value">27,61</div>
              </div>
              <div class="item item-2">
                <div class="value">30,71</div>
              </div>
              <div class="item item-3">
                <div class="value">30,75</div>
              </div>
              <div class="item item-4">
                <div class="value">32,80</div>
              </div>

            </div>
          </div>
        </header>
        <div class="subsection subsection-content" data-price="55000">
          <div class="content">
            <div class="items">
              <!-- Квартира 1 -->
              <div class="item item-1">
                <div class="col col-1">
                  <div class="pic">
                    <a href="images/plans/27-61.png" class="colorbox" rel="plans" title="Планировка квартиры площадью 27,61 кв. м.">
                      <img src="images/plans/27-61.png" alt="">
                      <span class="zoom">Увеличить</span>
                    </a>
                  </div>
                </div>
                <div class="col col-2">
                  <h3>Характеристики</h3>
                  <div class="char-table">
                    <div class="row">
                      <div class="name">Жилая зона</div>
                      <div class="value">20.13</div>
                    </div>
                    <div class="row">
                      <div class="name">Прихожая</div>
                      <div class="value">3.08</div>
                    </div>
                    <div class="row">
                      <div class="name">Ванная и с/у</div>
                      <div class="value">3.25</div>
                    </div>
                    <div class="row">
                      <div class="name">Балкон</div>
                      <div class="value">3.82</div>
                    </div>
                  </div>
                  <a href="#modal-form-view" class="btn popupper">Записаться на просмотр</a>
                </div>
                <div class="col col-3">
                  <h3>Стоимость</h3>
                  <div class="price-item">
                    <div class="unit">1 м<sup>2</sup></div>
                    <div class="value">42 000</div>
                  </div>
                  <div class="price-item">
                    <div class="unit">Квартира</div>
                    <div class="value">1 159 620</div>
                  </div>
                </div>
              </div>
              <!-- // Квартира 1 -->
              <!-- Квартира 2 -->
              <div class="item item-2">
                <div class="col col-1">
                  <div class="pic">
                    <a href="images/plans/30-71.png" class="colorbox" rel="plans" title="Планировка квартиры площадью 30,71 кв. м.">
                      <img src="images/plans/30-71.png" alt="">
                      <span class="zoom">Увеличить</span>
                    </a>
                  </div>
                </div>
                <div class="col col-2">
                  <h3>Характеристики</h3>
                  <div class="char-table">
                    <div class="row">
                      <div class="name">Жилая зона</div>
                      <div class="value">23.17</div>
                    </div>
                    <div class="row">
                      <div class="name">Прихожая</div>
                      <div class="value">3.08</div>
                    </div>
                    <div class="row">
                      <div class="name">Ванная и с/у</div>
                      <div class="value">3.25</div>
                    </div>
                    <div class="row">
                      <div class="name">Балкон</div>
                      <div class="value">4.04</div>
                    </div>
                  </div>
                  <a href="#modal-form-view" class="btn popupper">Записаться на просмотр</a>
                </div>
                <div class="col col-3">
                  <h3>Стоимость</h3>
                  <div class="price-item">
                    <div class="unit">1 м<sup>2</sup></div>
                    <div class="value">42 000</div>
                  </div>
                  <div class="price-item">
                    <div class="unit">Квартира</div>
                    <div class="value">1 289 820</div>
                  </div>
                </div>
              </div>
              <!-- // Квартира 2 -->
              <!-- Квартира 3 -->
              <div class="item item-3">
                <div class="col col-1">
                  <div class="pic">
                    <a href="images/plans/30-75.png" class="colorbox" rel="plans" title="Планировка квартиры площадью 30,75 кв. м.">
                      <img src="images/plans/30-75.png" alt="">
                      <span class="zoom">Увеличить</span>
                    </a>
                  </div>
                </div>
                <div class="col col-2">
                  <h3>Характеристики</h3>
                  <div class="char-table">
                    <div class="row">
                      <div class="name">Жилая зона</div>
                      <div class="value">23.17</div>
                    </div>
                    <div class="row">
                      <div class="name">Прихожая</div>
                      <div class="value">3.08</div>
                    </div>
                    <div class="row">
                      <div class="name">Ванная и с/у</div>
                      <div class="value">3.25</div>
                    </div>
                    <div class="row">
                      <div class="name">Балкон</div>
                      <div class="value">4.15</div>
                    </div>
                  </div>
                  <a href="#modal-form-view" class="btn popupper">Записаться на просмотр</a>
                </div>
                <div class="col col-3">
                  <h3>Стоимость</h3>
                  <div class="price-item">
                    <div class="unit">1 м<sup>2</sup></div>
                    <div class="value">42 000</div>
                  </div>
                  <div class="price-item">
                    <div class="unit">Квартира</div>
                    <div class="value">1 291 500</div>
                  </div>
                </div>
              </div>
              <!-- // Квартира 3 -->
              <!-- Квартира 4 -->
              <div class="item item-4">
                <div class="col col-1">
                  <div class="pic">
                    <a href="images/plans/32-80.png" class="colorbox" rel="plans" title="Планировка квартиры площадью 32,80 кв. м.">
                      <img src="images/plans/32-80.png" alt="">
                      <span class="zoom">Увеличить</span>
                    </a>
                  </div>
                </div>
                <div class="col col-2">
                  <h3>Характеристики</h3>
                  <div class="char-table">
                    <div class="row">
                      <div class="name">Жилая зона</div>
                      <div class="value">20.51</div>
                    </div>
                    <div class="row">
                      <div class="name">Прихожая</div>
                      <div class="value">5.82</div>
                    </div>
                    <div class="row">
                      <div class="name">Ванная и с/у</div>
                      <div class="value">5.11</div>
                    </div>
                    <div class="row">
                      <div class="name">Балкон</div>
                      <div class="value">4.52</div>
                    </div>
                  </div>
                  <a href="#modal-form-view" class="btn popupper">Записаться на просмотр</a>
                </div>
                <div class="col col-3">
                  <h3>Стоимость</h3>
                  <div class="price-item">
                    <div class="unit">1 м<sup>2</sup></div>
                    <div class="value">42 000</div>
                  </div>
                  <div class="price-item">
                    <div class="unit">Квартира</div>
                    <div class="value">1 377 600</div>
                  </div>
                </div>
              </div>
              <!-- // Квартира 4 -->
              <!-- Квартира 5 -->
              <div class="item item-5 selected">
                <div class="col col-1">
                  <div class="pic">
                    <a href="images/plans/43-80.png" class="colorbox" rel="plans" title="Планировка квартиры площадью 43,80 кв. м.">
                      <img src="images/plans/43-80.png" alt="">
                      <span class="zoom">Увеличить</span>
                    </a>
                  </div>
                </div>
                <div class="col col-2">
                  <h3>Характеристики</h3>
                  <div class="char-table">
                    <div class="row">
                      <div class="name">Жилая зона</div>
                      <div class="value">18.58</div>
                    </div>
                    <div class="row">
                      <div class="name">Кухня</div>
                      <div class="value">11.90</div>
                    </div>
                    <div class="row">
                      <div class="name">Прихожая</div>
                      <div class="value">8.44</div>
                    </div>
                    <div class="row">
                      <div class="name">Ванная и с/у</div>
                      <div class="value">3.67</div>
                    </div>
                    <div class="row">
                      <div class="name">Балкон</div>
                      <div class="value">4.02</div>
                    </div>
                  </div>
                  <a href="#modal-form-view" class="btn popupper">Записаться на просмотр</a>
                </div>
                <div class="col col-3">
                  <h3>Стоимость</h3>
                  <div class="price-item">
                    <div class="unit">1 м<sup>2</sup></div>
                    <div class="value">42 000</div>
                  </div>
                  <div class="price-item">
                    <div class="unit">Квартира</div>
                    <div class="value">1 839 600</div>
                  </div>
                </div>
              </div>
              <!-- // Квартира 5 -->
              <!-- Квартира 6 -->
              <div class="item item-6">
                <div class="col col-1">
                  <div class="pic">
                    <a href="images/plans/76-23.png" class="colorbox" rel="plans" title="Планировка квартиры площадью 76,23 кв. м.">
                      <img src="images/plans/76-23.png" alt="">
                      <span class="zoom">Увеличить</span>
                    </a>
                  </div>
                </div>
                <div class="col col-2">
                  <h3>Характеристики</h3>
                  <div class="char-table">
                    <div class="row">
                      <div class="name">Жилая зона</div>
                      <div class="value">54.34</div>
                    </div>
                    <div class="row">
                      <div class="name">Кухня</div>
                      <div class="value">13.47</div>
                    </div>
                    <div class="row">
                      <div class="name">Прихожая</div>
                      <div class="value">14.42</div>
                    </div>
                    <div class="row">
                      <div class="name">Ванная и с/у</div>
                      <div class="value">3.47 + 1.83</div>
                    </div>
                    <div class="row">
                      <div class="name">Балкон</div>
                      <div class="value">3.87 + 3.38</div>
                    </div>
                  </div>
                  <a href="#modal-form-view" class="btn popupper">Записаться на просмотр</a>
                </div>
                <div class="col col-3">
                  <h3>Стоимость</h3>
                  <div class="price-item">
                    <div class="unit">1 м<sup>2</sup></div>
                    <div class="value">42 000</div>
                  </div>
                  <div class="price-item">
                    <div class="unit">Квартира</div>
                    <div class="value">3 231 060</div>
                  </div>
                </div>
              </div>
              <!-- // Квартира 6 -->
            </div>
          </div>
        </div>
      </section>
      <section class="section-predetails"></section>
      <section id="section-9" class="section-details">
        <div class="content">
          <h2>Важные детали</h2>
          <div class="inner">
            <div class="left">
              <p>Поселившись в ЖК «Луч», вы не раз убедитесь, как легко жить с комфортом и радостью.</p>
              <p>Например, через дорогу от дома есть сквер — идеальный для пробежки или прогулки с любимой собакой. А владельцев велосипедов порадует удобная велопарковка у дома. </p>
              <p>Благодаря множеству продуманных деталей жизнь здесь становится приятнее, а вы сами — счастливее!</p>
            </div>
            <div class="right">
              <p>Жилой комплекс расположен в перспективном районе ЗИП, по соседству с новым микрорайоном «Московский».</p>
              <p>Школы, садики, «Семейный магнит» и другие магазины, автобусные и трамвайные остановки — всё это рядом.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="section-infrastructure" id="infrastructure">
        <div class="map-wrapper">
          <div class="content">
            <div class="map-legend">
              <div class="items">
                <div class="item item-all selected">
                  <div class="icon"></div>
                  <div class="name">Все объекты</div>
                </div>
                <div class="item item-school">
                  <div class="icon"></div>
                  <div class="name">Образование</div>
                </div>
                <div class="item item-shop">
                  <div class="icon"></div>
                  <div class="name">Покупки</div>
                </div>
                <div class="item item-sport">
                  <div class="icon"></div>
                  <div class="name">Спорт</div>
                </div>
                <div class="item item-hospital">
                  <div class="icon"></div>
                  <div class="name">Здоровье</div>
                </div>
                <div class="item item-park">
                  <div class="icon"></div>
                  <div class="name">Отдых</div>
                </div>
              </div>
            </div>
          </div>
          <div id="map" class="map"></div>
        </div>
      </section>

      <section id="stages" class="section-stages">
        <div class="content">
          <h2>Этапы строительства</h2>
          <div class="line-wrapper">
            <div class="stages-dates date-start">Июнь 2015</div>
            <div class="ready-wrapper">
              <div class="ready-line">
                <div class="ready-value">45</div>
              </div>
            </div>
            <div class="stages-dates date-finish">4 квартал 2016</div>
          </div>
          <div class="stages-wrapper">
            <?php print get_gallery(); ?>
          </div>
          <div class="stripe">
            <div class="left">Увидеть
              <br>своими глазами</div>
            <div class="right">
              <form action="" id="stages-form">
                <div class="input-wrapper">
                  <input id="stages-form-tel" type="tel" name="phone">
                  <label for="stages-form-tel">Телефон</label>
                  <div class="border"></div>
                </div>
                <input type="submit" value="Заказать звонок" class="btn yellow">
              </form>
            </div>
          </div>
        </div>
      </section>


      <section class="section-variants" id="how-to-buy">
        <div class="content">
          <div class="title-wrapper">
            <h2>Как купить ?</h2>
            <p class="description">Правила компании «Остринский» — индивидуальный подход к каждому
              <br>клиенту и юридическая чистота сделок. Выберите
              <br>оптимальный вариант оплаты, и мы сделаем всё необходимое.</p>
          </div>
          <div class="tabs-wrapper">
            <div class="btns">
              <a href="#" class="btn selected">Ипотека</a>
              <a href="#" class="btn">Рассрочка</a>
              <a href="#" class="btn">Маткапитал</a>
              <a href="#" class="btn">Дистанционная покупка</a>
            </div>
            <div class="tabs">
              <div class="tab tab-1 selected">
                <div class="banks">
                  <img src="images/logo-sberbank.png" alt="Сбербанк">
                  <img src="images/logo-vtb.png" alt="ВТБ 24">
                  <img src="images/logo-bankofmoscow.png" alt="Банк Москвы">
                  <img src="images/logo-mib.png" alt="Московский индустриальный банк">
                  <img src="images/logo-kia.png" alt="Кубанское ипотечное агентство">
                </div>
                <div class="center"><a href="#modal-form-ipoteque" class="btn popupper blue">Оставить заявку</a></div>
              </div>
              <div class="tab tab-2">
                <h3>Рассрочка – это практично</h3>
                <p>Покупка квартиры в рассрочку помогает избежать серьезных перемен в вашем бюджете. Выплачивайте понемногу и живите, как привыкли.</p>
                <div class="table">
                  <div class="table-caption">Условия рассрочки платежей</div>
                  <div class="table-head">
                    <div class="table-cell">на 1 год</div>
                    <div class="table-cell">на 2 года</div>
                  </div>
                  <div class="table-row">
                    <div class="table-cell">1 взнос – 50%</div>
                    <div class="table-cell">1 взнос – 50%</div>
                  </div>
                  <div class="table-row">
                    <div class="table-cell">Остальные платежи равными долями ежемесячно или поквартально без удорожания</div>
                    <div class="table-cell">Остальные платежи: 1 год-25% равными долями без удорожания.
                      <br>2 год удорожание на 15% на остаток суммы</div>
                  </div>
                  <div class="table-row">
                    <div class="table-cell"></div>
                    <div class="table-cell"></div>
                  </div>
                </div>
                <p class="center">Уточните все детали у специалиста!</p>
                <div class="center"><a href="#modal-form-rassrochka" class="btn popupper blue">Оставить заявку</a></div>
              </div>
              <div class="tab tab-3">
                <p>Сумма материнского капитала принимается:
                <ul>
                  <li>в счет первоначального взноса при ипотечном кредитовании,</li>
                  <li>в счет досрочного погашения ипотечного кредита,</li>
                  <li>для приобретения квартиры без кредитных средств.</li>
                </ul>
                </p>
                <div class="center"><a href="#modal-form-motherhood" class="btn popupper blue">Оставить заявку</a></div>
              </div>
              <div class="tab tab-4">
                <p>Для иногородних клиентов предусмотрено дистанционное обслуживание. Это значит, что, даже находясь в другом городе, вы сможете без труда оформить приобретение жилья</p>
                <div class="center"><a href="#modal-form-distance" class="btn popupper blue">Оставить заявку</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="about" class="section-company">
        <div class="content">
          <div class="row">
            <div class="left">
              <h2>О застройщике</h2>
              <p>Строительно-инвестиционная компания «Остринский»
                <br>с 2012 г. создает качественное жилье в Краснодаре и Сочи. Мы специализируемся на форматах «бизнес» и «бизнес+». Строим технологично, в срок, в соответствии с вашими ожиданиями и законом. Сейчас ведем три краснодарских проекта жилой недвижимости:</p>
              <p>— ЖК «Заря» на ул. Монтажников, 1/2;
                <br>— ЖК «Луч» на ул. Солнечной, 18/5;
                <br>— ЖК «Мир» в р-не «Красной площади».</p>
            </div>
            <div class="right">
              <div class="pic"></div>
              <div class="pic-description">139 822 м<sup>2</sup></div>
              <p>Жилплощади построено с&nbsp;2012&nbsp;г.</p>
            </div>
          </div>
          <div class="docs">
            <h3>Документация</h3>
            <div class="doc-wrapper">
              <?php /*<a href="files/registration.pdf" target=_blank><img src="images/doc-1.jpg" alt=""><span>Свидетельство о государственной регистрации права</span></a>*/?>
              <a href="docs/declaration.pdf" target=_blank><img src="images/doc-2.jpg" alt=""><span>Проектная декларация</span></a>
              <a href="docs/allowance corrected(03.11.15).pdf" target=_blank><img src="images/doc-3.jpg" alt=""><span>Разрешение на строительство</span></a>
              <a href="docs/expertise.pdf" target=_blank><img src="images/doc-4.jpg" alt=""><span>Экспертиза</span></a>
            </div>
          </div>
        </div>
      </section>
      <section class="section-contacts" id="contacts">
        <div class="content">
          <div class="pre-title-title-ha-ha-ha">
            «Луч» улучшает условия жизни.
            <br>Решайтесь на перемены!
          </div>
          <h2><span>Контакты</span></h2>
          <div class="cols">
            <div class="col col-1">
              <p>Отдел продаж
                <br>
                <a href="tel:<? echo $phone['number']; ?>"><? echo $phone['number']; ?></a>
              </p>
            </div>
            <div class="col col-2">
              <p>г. Краснодар
                <br>ул. Дальняя, 39/3 (2 эт.)</p>
            </div>
            <div class="col col-3">
              <a href="mailto:tatyana.d@ostrinskiy.com">tatyana.d@ostrinskiy.com</a>
              <ul class="socials">
                <li><a href="https://vk.com/jkluch23" class="vk" target=_blank>вКонтакте</a></li>
                <li><a href="https://www.facebook.com/jkluch93?fref=ts" class="fb" target=_blank>Facebook</a></li>
                <li><a href="https://www.instagram.com/jkluch23/" class="in" target=_blank>Instagram</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="section-digestive">
        <div class="content">
          <p>Заполните форму, и мы свяжемся с вами для уточнения информации.</p>
          <form id="digestive-form" action="send.php">
            <div class="input-wrapper">
              <input type="text" id="digestive-form-name" name="name">
              <label for="digestive-form-name">Имя</label>
              <div class="border"></div>
            </div>
            <div class="input-wrapper">
              <input type="tel" id="digestive-form-tel" name="phone">
              <label for="digestive-form-tel">Телефон</label>
              <div class="border"></div>
            </div>
            <input type="submit" value="Заказать звонок" class="btn yellow">
          </form>
        </div>
      </section>
    </main>
    <footer>
      <div class="content">
        <div class="copyright">&copy; 2015, Строительно-инвестиционная компания &laquo;Остринский&raquo;</div>
        <div class="studio">
          <a href="http://southmedia.ru/" target="_blank" rel="nofollow" class="developed-by">
            <span class="developed-by__text">Разработано в агентстве</span>
            <img src="images/bg-development.png" alt="Southmedia Создание и продвижение сайта" title="Southmedia Создание и продвижение сайта">
          </a>
        </div>
      </div>
    </footer>
    <div class="modal" id="modal-callback-header">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Обратный звонок</h2>
        <form id="form-callback-header" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="form-callback-header-phone">
            <label for="form-callback-header-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-ask-more">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Узнать больше</h2>
        <form id="form-ask-more" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="form-ask-more-phone">
            <label for="form-ask-more-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-view">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Записаться<br>на просмотр</h2>
        <form id="form-view" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-form-view-phone">
            <label for="modal-form-view-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-ipoteque">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Оставить заявку</h2>
        <form id="form-ipoteque" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-form-ipoteque-phone">
            <label for="modal-form-ipoteque-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-rassrochka">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Оставить заявку</h2>
        <form id="form-rassrochka" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-rassrochka-ipoteque-phone">
            <label for="modal-form-rassrochka-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-motherhood">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Оставить заявку</h2>
        <form id="form-motherhood" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-form-motherhood-phone">
            <label for="modal-form-motherhood-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-distance">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Оставить заявку</h2>
        <form id="form-distance" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-form-distance-phone">
            <label for="modal-form-distance-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-high-class">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Узнать больше</h2>
        <form id="form-high-class" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-form-high-class-phone">
            <label for="modal-form-high-class-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="modal-form-closed-territory">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <h2>Заказать звонок</h2>
        <form id="form-closed-territory" action="send.php" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="Телефон" id="modal-form-closed-territory-phone">
            <label for="modal-form-closed-territory-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <div class="markup">Задать вопрос, купить или забронировать<br>квартиру можно по телефону.<br>Мы позвоним вам сами.</div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Отправить" class="btn">
        </form>
      </div>
    </div>
    <div class="modal" id="menu">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <nav>
          <ul>
            <li><a href="#section-1">О Комплексе</a></li>
            <li><a href="#section-3">В квартирах</a></li>
            <li><a href="#section-4">В доме</a></li>
            <?php /*<li><a href="#section-8">Планировки</a></li>*/?>
            <li><a href="#section-9">Важные детали</a></li>
            <li><a href="#infrastructure">Инфраструктура</a></li>
            <li><a href="#how-to-buy">Как купить</a></li>
            <li><a href="#about">О застройщике</a></li>
            <li><a href="#contacts">Контакты</a></li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="modal modal-intro" id="modal-fasade">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <div class="modal-intro-header">
          <img src="images/modal-fasade.png" alt="Вентилируемый фасад">
          <h2>Вентилируемый фасад</h2>
        </div>
        <div class="modal-intro-content">
          <p>Летом дом не перегревается на южном солнце,
            а зимой превосходно сберегает внутреннее тепло. Повышенная звукоизоляция избавляет
            от лишнего шума, а отделка из керамогранита&nbsp;— от сомнений в износоустойчивости жилья.</p>
        </div>
      </div>
    </div>
    <div class="modal modal-intro" id="modal-firm-walls">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <div class="modal-intro-header">
          <img src="images/modal-walls.png" alt="Прочные стены">
          <h2>Прочные стены</h2>
        </div>
        <div class="modal-intro-content">
          <p>Стены состоят из монолита и газоблока, поэтому их не нужно выравнивать; они пожаробезопасны и экологичны. Дома комфортно дышится и вам, и вашим детям.</p>
        </div>
      </div>
    </div>
    <div class="modal modal-intro" id="modal-cosy-plans">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <div class="modal-intro-header">
          <img src="images/modal-plans.png" alt="Удобные планировки">
          <h2>Удобные планировки</h2>
        </div>
        <div class="modal-intro-content">
          <p>«Луч» – пространство не только для солнечной жизни, но и для творчества. Посмотрите все варианты планировки, чтобы найти идеальную для себя, а потом воплощайте в солнечном интерьере авторские дизайнерские идеи!</p>
        </div>
      </div>
    </div>
    <div class="modal modal-intro" id="modal-porches">
      <a href="#" class="close-btn"></a>
      <div class="modal-content">
        <div class="modal-intro-header">
          <img src="images/modal-porches.png" alt="Современные подъезды">
          <h2>Современные подъезды</h2>
        </div>
        <div class="modal-intro-content">
          <p>Просторный и светлый подъезд, бесшумно работающие лифты и мусоропроводная система для экономии времени – это не мелочи, а новые поводы гордиться своим домом. Уверены, что вам хоть чуть-чуть позавидует каждый гость.</p>
        </div>
      </div>
    </div>



<div class="modal_popup">
   <div class="close_modal_back"  onclick="$(this).parent().fadeOut();" ></div>
    <div class="modal_block">
        <span class="close_button"  onclick="$(this).parent().parent().fadeOut();" ></span>
        <form id="form-modal" action="" autocomplete="off" novalidate>
          <div class="input-wrapper">
            <input type="tel" name="phone" class="form-text" placeholder="" id="modal-form-modal-phone">
            <label for="modal-form-modal-phone">Телефон</label>
            <div class="border"></div>
          </div>
          <input type="text" name="first_name" class="skrito" placeholder="First name">
          <input type="submit" value="Заказать звонок" class="btn submit_modal">
        </form>
        <p class="thnks">Спасибо. Мы перезвоним в ближайшее время!</p>
    </div>
</div>



    <div class="overlay"></div>
    <!--[if lt IE 7]>
        <div style='height: 59px; padding:0 0 0 15px; position: absolute; top: 0; z-index: 9999;'>
            <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
                <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0016_russian.jpg" border="0" height="42" width="820" alt="Вы используете устаревшую версию браузера Internet Explorer. Выполните обновление для более быстрой и безопасной работы с веб-страницами."/>
            </a>
        </div>
        <![endif]-->
    <script src="js/detect.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/waypoint.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.counterup.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.maskedinput.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://maps.googleapis.com/maps/api/js?signed_in=true&libraries=places&sensor=true"></script>
    <script src="js/infobubble.js"></script>
    <script src="js/jquery.colorbox-min.js"></script>
    <script src="js/jquery.colorbox-ru.js"></script>
    <script src="js/map.js"></script>
    <script src="js/parallaxy.js"></script>
    <script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script src="js/main.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/js.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">


        var opened = false;
        var sending = false
        function jivo_onOpen() { opened = true; }

        setInterval(function() {
            var user = jivo_api.getContactInfo();
         console.log('check');
            if(user.phone !== undefined && user.phone.length > 9 && opened && !sending && getCookie('var_ostrinskiy') != 'sended') {
                $.ajax({
                    type: "POST",
                    url: "http://ostrinskiy23.ru/tob24.php",
                    data: user,
                    success: function(msg){
                        sending = true;
                        setCookie('var_ostrinskiy', 'sended', {'expires' : 3600 * 24 * 7});
                        return null;
                    }
                });
            }
        }, 10000);

        function getCookie(name) {
         var matches = document.cookie.match(new RegExp(
         "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
         ));
         return matches ? decodeURIComponent(matches[1]) : undefined;
        }

        function setCookie(name, value, options) {
         options = options || {};

         var expires = options.expires;

         if (typeof expires == "number" && expires) {
         var d = new Date();
         d.setTime(d.getTime() + expires * 1000);
         expires = options.expires = d;
         }
         if (expires && expires.toUTCString) {
         options.expires = expires.toUTCString();
         }

         value = encodeURIComponent(value);

         var updatedCookie = name + "=" + value;

         for (var propName in options) {
         updatedCookie += "; " + propName;
         var propValue = options[propName];
         if (propValue !== true) {
         updatedCookie += "=" + propValue;
         }
         }

         document.cookie = updatedCookie;
        }
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter34081905 = new Ya.Metrika({
                        id:34081905,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/34081905" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-70136788-4', 'auto');
    ga('send', 'pageview');

    </script>


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 937895159;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/937895159/?value=0&guid=ON&script=0"/>
</div>
</noscript>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'fJ1XNIN37U';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=Ik85qh2WY6h*rSxdZi6iWZeNNGM6izSQddBz9AYjL8b8lCEfzgW7SiJXxjkAtjbTXjibbuTpgwnnYmXWAbp*yz6sZJ6gEGOi5r8UMhiihFTFFeXnEFtEKevhLespQdF3jllnY/e1*qv1D/vDq2fsTlPDlLRt6xl0TqNLHiqQl5w-';</script>

  </body>

</html>
